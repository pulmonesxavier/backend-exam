"""
Global wsgi class
"""
from parse import parse
from webob import Request, Response

class LIG:

    def __init__(self):
        #initializes routes with paths as the key and handlers as values
        self.routes = {}

    def __call__(self, environ, start_response):
        request = Request(environ)
        response = self.handle_request(request)

        return response(environ, start_response)

    def handle_request(self, request):
        # initial handling of request
        response = Response()

        # cors-headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = '*'
        response.headers['Access-Control-Allow-Methods'] = '*'

        handler, kwargs = self.find_handler(request_path=request.path)

        # class-based handlers
        handler = getattr(handler(), request.method.lower(), None)

        if handler is not None:
            handler(request, response, **kwargs)
        else:
            self.default_response(response)

        return response

    def route(self, path, handler):
        # stores the path as a key and handler as a value
        self.routes[path] = handler
        return handler

    def default_response(self, response):
        response.status_code = 404
        response.text = "Not found."

    def find_handler(self, request_path):
        #search for the path and handler for the request
        for path, handler in self.routes.items():
            parse_result = parse(path, request_path)
            if parse_result:
                return handler, parse_result.named
            # if path == request_path:
            #     return handler
        return None, None
