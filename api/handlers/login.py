"""
Handles registration requests
"""

import os
import hashlib
import datetime
import uuid

import psycopg2
from utils.password_security import verify_password

class Login:

    def options(self, request, response):
        # left blank for cors
        pass

    def post(self, request, response):
        errors = {}
        email = request.json["email"]

        sql = """SELECT password FROM users WHERE email = %s;"""
        conn = None
        row = None

        try:
            conn = psycopg2.connect(
                host="localhost",
                database="lig",
                user="liguser",
                password="liguser")
            cur = conn.cursor()
            cur.execute(sql, (email,))
            row = cur.fetchone()[0]
            cur.close()
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        if row is None:
            errors.setdefault("email",[]).append("These credentials do not match our records.")
        else:
            if not verify_password(row,request.json["password"]):
                errors.setdefault("password",[]).append("These credentials do not match our records.")
        if not email:
            errors.setdefault("email",[]).append("The email field is required.")
        if not request.json["password"]:
            errors.setdefault("password",[]).append("The password field is required.")

        print(errors)

        if not errors:
            token = str(uuid.uuid4().hex)
            expires_at = str(datetime.datetime.now() + datetime.timedelta(days=7))

            sql = """INSERT INTO tokens(token, expires_at)
            VALUES (%s,%s)"""
            conn = None

            try:
                conn = psycopg2.connect(
                    host="localhost",
                    database="lig",
                    user="liguser",
                    password="liguser")
                cur = conn.cursor()
                cur.execute(sql, (token, expires_at,))
                cur.close()
                conn.commit()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error)
            finally:
                if conn is not None:
                    conn.close()
            response.json = {
                "token": token,
                "token_type": "bearer",
                "expires_at": expires_at
                }
        else:
            response.status = "422 Unprocessable Entity"
            response.json = {
                "message": "The given data was invalid.",
                "errors": errors
            }
