"""
Handles registration requests
"""

import os
import hashlib
import datetime

import psycopg2

class Posts:
    def options(self, request, response):
        pass

    def get(self, request, response):
        posts = []
        sql = """SELECT * FROM posts;"""
        conn = None
        try:
            conn = psycopg2.connect(
                host="localhost",
                database="lig",
                user="liguser",
                password="liguser")
            cur = conn.cursor()
            cur.execute(sql)
            rows = cur.fetchall()
            for row in rows:
                posts.append({
                    "id": row[0],
                    "user_id": row[1],
                    "title": row[2],
                    "slug": row[3],
                    "content": row[4],
                    "created_at": str(row[5]),
                    "updated_at": str(row[6]),
                    "deleted_at": "null"
                })
            cur.close()
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        response.json = {
            "data": posts,
            "links": {
                "first": "http://127.0.0.1:8000/api/posts?page=1",
                "last": "http://127.0.0.1:8000/api/posts?page=1",
                "prev": "null",
                "next": "null"
            },
            "meta": {
                "current_page": 1,
                "from": 1,
                "last_page": 1,
                "path": "http://127.0.0.1:8000/api/posts",
                "per_page": 15,
                "to": 1,
                "total": 1
            }
        }

class Post:
    def get(self, request, response, post):
        sql = """SELECT * FROM posts WHERE slug = %s;"""
        conn = None
        row = None

        try:
            conn = psycopg2.connect(
                host="localhost",
                database="lig",
                user="liguser",
                password="liguser")
            cur = conn.cursor()
            cur.execute(sql, (post,))
            row = cur.fetchone()
            cur.close()
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        if row is None:
            response.status = "404 Not Found"
            response.json = {
                "message": "No query results for model [App\\Post]."
            }
        else:
            response.json = {
                "data": {
                    "id": row[0],
                    "user_id": row[1],
                    "title": row[2],
                    "slug": row[3],
                    "content": row[4],
                    "created_at": str(row[5]),
                    "updated_at": str(row[6]),
                    "deleted_at": "null"
                }
            }
    def options(self, request, response, post):
        pass

    def patch(self, request, response, post):
        sql = """SELECT * FROM tokens WHERE token = %s;"""
        conn = None
        row = None
        token = request.authorization.params
        try:
            conn = psycopg2.connect(
                host="localhost",
                database="lig",
                user="liguser",
                password="liguser")
            cur = conn.cursor()
            cur.execute(sql, (token,))
            row = cur.fetchone()[1]
            cur.close()
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
        if row is None:
            response.status = "404 Not Found"
        else:

            sql = """UPDATE posts
                    SET title = %s, content = %s, slug = %s, updated_at = %s
                    WHERE slug = %s RETURNING *"""
            conn = None
            try:
                conn = psycopg2.connect(
                    host="localhost",
                    database="lig",
                    user="liguser",
                    password="liguser")
                cur = conn.cursor()
                cur.execute(sql, (request.json["title"],request.json["content"],request.json["slug"],str(datetime.datetime.now()),post,))
                row = cur.fetchone()
                cur.close()
                conn.commit()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error)
            finally:
                if conn is not None:
                    conn.close()

            response.json = {
                "data": {
                    "id": row[0],
                    "user_id": row[1],
                    "title": row[2],
                    "slug": row[3],
                    "content": row[4],
                    "created_at": str(row[5]),
                    "updated_at": str(row[6]),
                    "deleted_at": "null"
                }
            }
