"""
Handles registration requests
"""

import os
import hashlib
import datetime

import psycopg2
from utils.password_security import hash_password

class Register:

    def options(self, request, response):
        # left blank for cors
        pass

    def post(self, request, response):
        errors = {}
        email = request.json["email"]

        sql = """SELECT password FROM users WHERE email = %s;"""
        conn = None
        row = None

        try:
            conn = psycopg2.connect(
                host="localhost",
                database="lig",
                user="liguser",
                password="liguser")
            cur = conn.cursor()
            cur.execute(sql, (email,))
            row = cur.fetchone()[0]
            cur.close()
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

        if row is not None:
            errors.setdefault("email",[]).append("The email has already been taken.")
        if not request.json["email"]:
            errors.setdefault("email",[]).append("The email field is required.")
        if not request.json["name"]:
            errors.setdefault("name",[]).append("The name field is required.")
        if not request.json["password"]:
            errors.setdefault("password",[]).append("The password field is required.")
        if (request.json["password"] != request.json["password_confirmation"]):
            errors.setdefault("password",[]).append("The password confirmation does not match.")

        if not errors:
            password = hash_password(request.json["password"])
            now = str(datetime.datetime.now())

            sql = """INSERT INTO users(name, password, email, created_at, updated_at)
            VALUES (%s,%s, %s, %s, %s) RETURNING user_id;"""
            conn = None
            user_id = None

            try:
                conn = psycopg2.connect(
                    host="localhost",
                    database="lig",
                    user="liguser",
                    password="liguser")
                cur = conn.cursor()
                cur.execute(sql, (request.json["name"], password, email, now, now,))
                user_id = cur.fetchone()[0]
                cur.close()
                conn.commit()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error)
            finally:
                if conn is not None:
                    conn.close()


            # execute_db(sql, (request.json["name"], password, email, now, now,))
            response.json = {
                "name": request.json["name"],
                "email": email,
                "updated_at": now,
                "created_at": now,
                "id": user_id,
            }
        else:
            response.status = "422 Unprocessable Entity"
            response.json = {
                "message": "The given data was invalid.",
                "errors": errors
            }
