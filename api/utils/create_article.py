import psycopg2
import datetime

def create_articles(article_list):
    """ create articles in the PostgreSQL database"""

    sql = "INSERT INTO posts(user_id, title, slug, content, created_at, updated_at) VALUES(%s, %s, %s, %s, %s, %s)"
    conn = None

    try:
        # read the connection parameters
        # connect to the PostgreSQL server
        conn = psycopg2.connect(
            host="localhost",
            database="lig",
            user="liguser",
            password="liguser")
        cur = conn.cursor()
        # create table one by one
        cur.executemany(sql,article_list)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    now = str(datetime.datetime.now())
    create_articles([
        (3, "Test Title", "test-title", "Test Content",now,now),
        (3, "Test Title2", "test-title-2", "Test Content 2",now,now),
        (14, "Test Title3", "test-title-3", "Test Content 3",now,now),
        (14, "Test Title4", "test-title-4", "Test Content 4",now,now),
        (14, "Test Title5", "test-title-5", "Test Content 5",now,now),
    ])