"""
Entry point and collection of routes
"""

from server import LIG

from handlers.login import *
from handlers.posts import *
from handlers.register import *

app = LIG()

app.route('/api/register', Register)
app.route('/api/login', Login)
app.route('/api/posts', Posts)
app.route('/api/posts/{post}', Post)

