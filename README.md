Examination Rules:

- Use Laravel PHP Framework
- You can bring your own laptop or use provided MacBook Air
- Use of internet is allowed
- Asking for help is not allowed
- Exam time is 3 hours, submitting early will earn bonus points

Steps:
- Read API documentation here (https://documenter.getpostman.com/view/78990/RznLHGgv)
- Fork frontend application here (https://gitlab.com/ligph.com/backend-exam) to your repository
- Put your files in the `/api` folder
- Implement requested API endpoints
- Run your api server in http://localhost:8000
- Run the FE server using `npm i` and `npm start`. It will automatically open in http://localhost:3000
- Push your changes to your repository
- Submit pull request to the main repository master

Scoring:
Scoring is based on:
- Completeness
- Technology used
- Coding style
Bonus:
- Time spent
- Migration
- Development Env
- Readme
- Git knowledge

## Setting up the backend
1. Make sure you have Python 3.8 installed ```python3 --version```. pyenv is recommended to manage multiple Python versions and make sure you don't use the system version.
2. Make sure you have PostgreSQL installed and running:```sudo apt-get install postgresql``` ```sudo service postgresql start```.
3. Create Postgres database using the Postgres interactive terminal:
```psql -d postgres
CREATE DATABASE lig;
CREATE USER liguser WITH ENCRYPTED PASSWORD 'liguser';
GRANT ALL PRIVILEGES ON DATABASE lig TO liguser;
```
4. Navigate into api directory: ```cd api```.
5. Run ```python3 -m venv env``` (creates virtual environment in current directory called 'env').
6. Run ```source env/bin/activate``` (activates the virtual environment).
7. Install psycog2 by running ```pip install psycopg2```
8. Install gunicorn by running ```pip install gunicorn```
9. Install webob by running ```pip install webob```
10. Install parse by running ```pip install parse```
11. Create initial tables using ```python create_table.py```
12. You are able to create posts/articles with ```python create_article.py```. But you'll need to change the user_id's  in utils/create_article.py to match with your user_ids.
13. Run the backend with ```gunicorn api:app```
14. The api server is now open at http://localhost:8000
